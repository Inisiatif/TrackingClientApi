# Changelog

All notable changes to `tracking-client-api` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## 2020-07-01

### Added
- Add `storeTrack` AND `addEvent`
- Add symplify/easy-coding-standard
- Add vimeo/psalm

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Remove squizlabs/php_codesniffer

### Security
- Nothing
