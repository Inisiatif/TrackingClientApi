<?php declare(strict_types=1);

namespace Inisiatif\TrackingClientApi;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;

final class ClientTest extends TestCase
{
    public function testCanTrackingQurbanWithEvent()
    {
        $params = [
            'type' => 'QURBAN',
            'source' => 'edonation',
            'source_id' => '163ae4d5-b774-11ea-9f04-0242ac120002',
            'meta' => [
                'identification_number' => '20200010101010',
                'name' => 'Nuradiyana',
                'amount' => 20000000,
                'payment_date' => '2020-06-29T03:43:54+00:00',
                'details' => [
                    ['id' => 1, 'amount' => 10000000, 'program' => '1 Sapi Qurban'],
                    ['id' => 2, 'amount' => 10000000, 'program' => '1/7 Sapi Qurban'],
                ],
            ],
            'events' => [
                [
                    'type' => 'CONFIRMATION',
                    'description' => 'Konfirmasi',
                    'date' => '2020-06-29',
                    'meta' => [],
                ],
            ],
        ];

        $httpClient = HttpClient::createForBaseUri('http://localhost:8080/');

        $client = new Client(
            new Credentials('admin@izi.or.id', 'password'), $httpClient
        );

        $response = $client->storeTrack($params);

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKey('id', $response['data']);
        $this->assertArrayHasKey('short_url', $response['data']);
    }

    public function testCanStoreEvent()
    {
        $params = [
            'tracking_id' => 'be1d6554-bb2c-11ea-9c92-0242ac120002', // TODO: Change this to generated not hardcode
            'type' => 'CONFIRMATION',
            'description' => 'Konfirmasi',
            'date' => '2020-06-29',
            'meta' => [],
        ];

        $httpClient = HttpClient::createForBaseUri('http://localhost:8080/');

        $client = new Client(
            new Credentials('admin@izi.or.id', 'password'), $httpClient
        );

        $response = $client->addEvent($params);

        var_dump($response);
    }
}
