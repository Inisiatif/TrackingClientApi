<?php

declare(strict_types=1);

namespace Inisiatif\TrackingClientApi;

interface ClientInterface
{
    public function storeTrack(array $params);

    public function addEvent(array $params);
}
