<?php declare(strict_types=1);

namespace Inisiatif\TrackingClientApi;

use Webmozart\Assert\Assert;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;

final class Client implements ClientInterface
{
    /**
     * @var Credentials
     */
    private $credentials;

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    public function __construct(Credentials $credentials, HttpClientInterface $httpClient)
    {
        $this->credentials = $credentials;
        $this->httpClient = $httpClient;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function storeTrack(array $params)
    {
        foreach (['type', 'source', 'source_id'] as $item) {
            Assert::keyExists($params, $item);
            Assert::notNull($params[$item]);
        }

        if (key_exists('events', $params)) {
            foreach ($params['events'] as $event) {
                foreach (['description', 'date', 'type'] as $item) {
                    Assert::keyExists($event, $item);
                    Assert::notNull($event[$item]);
                }
            }
        }

        $response = $this->httpClient->request('POST', '/api/v1/tracking', [
            'auth_basic' => $this->credentials->toBasicAuth(),
            'json' => $params,
        ]);

        return $response->toArray();
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function addEvent(array $params)
    {
        foreach (['description', 'date', 'type', 'tracking_id'] as $item) {
            Assert::keyExists($params, $item);
            Assert::notNull($params[$item]);
        }

        $response = $this->httpClient->request('POST', '/api/v1/event', [
            'auth_basic' => $this->credentials->toBasicAuth(),
            'json' => $params,
        ]);

        return $response->toArray();
    }
}
