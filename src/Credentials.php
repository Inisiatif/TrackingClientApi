<?php

declare(strict_types=1);

namespace Inisiatif\TrackingClientApi;

final class Credentials
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function toBasicAuth(bool $encode = false): string
    {
        $format = sprintf('%s:%s', $this->getUsername(), $this->getPassword());

        return $encode === false ? $format : base64_encode($format);
    }
}
