# TrackingClientApi

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

## Install

Via Composer

``` bash
$ composer require inisiatif/tracking-client-api
```

## Usage

See tests directory for usage example.

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer phpunit
```

## Security

If you discover any security related issues, please email nuradiyana@izi.or.id instead of using the issue tracker.

## Credits

- [Nuradiyana][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/inisiatif/tracking-client-api.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/inisiatif/tracking-client-api/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/inisiatif/tracking-client-api.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/inisiatif/tracking-client-api.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/inisiatif/tracking-client-api.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/inisiatif/tracking-client-api
[link-travis]: https://travis-ci.org/inisiatif/tracking-client-api
[link-scrutinizer]: https://scrutinizer-ci.com/g/inisiatif/tracking-client-api/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/inisiatif/tracking-client-api
[link-downloads]: https://packagist.org/packages/inisiatif/tracking-client-api
[link-author]: https://github.com/inisiatif
